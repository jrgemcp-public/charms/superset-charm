# superset-charm
A Juju charm which sets up and runs apache superset

Please see https://superset.apache.org for more information on the project

[![Get it from the Charmhub](https://charmhub.io/apache-superset/badge.svg)](https://charmhub.io/apache-superset)

## Deploying example data

If you'd like to try out apache superset with some pre-loaded data deploy like so : 

```
juju deploy postgresql
juju deploy apache-superset --config install_example_data=true
juju relate db:postgresql db:apache-superset
```