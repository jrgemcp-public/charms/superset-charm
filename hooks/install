#!/bin/bash
timedatectl set-timezone "$(config-get preferred_timezone)"
juju-log -l "WARNING" "Setting up SuperSet Installer"
status-set maintenance "Setting up SuperSet Installer $(date +"%H:%M")"
open-port 8088

apt-get update
apt-get -y upgrade
apt-get -y install build-essential libssl-dev libffi-dev python3-dev python3-pip libsasl2-dev libldap2-dev default-libmysqlclient-dev libpq-dev python-is-python3

juju-log -l "WARNING" "Apt dependencies installed"
status-set maintenance "Apt dependencies installed $(date +"%H:%M")"

pip install --upgrade pip
sudo -u ubuntu pip install --upgrade pip --no-warn-script-location
sudo -u ubuntu pip install werkzeug==2.0.3 --no-warn-script-location
sudo -u ubuntu pip install flask==2.1.3 --no-warn-script-location
sudo -u ubuntu pip install wtforms==2.3.0 --no-warn-script-location
sudo -u ubuntu pip install --upgrade pyopenssl --no-warn-script-location
sudo -u ubuntu pip install psycopg2-binary pillow gunicorn gevent --no-warn-script-location

sudo -u ubuntu pip install trino==0.318.0 thrift pyhive --no-warn-script-location

status-set maintenance "About to pip install apache-superset $(date +"%H:%M")"
juju-log -l "WARNING" "About to pip install apache-superset"
sudo -u ubuntu pip install apache-superset --no-warn-script-location
status-set maintenance "apache-superset installed, beginning db upgrade $(date +"%H:%M")"
juju-log -l "WARNING" "apache-superset installed, beginning db upgrade"

sudo touch /etc/profile.d/superset.sh
echo -e 'export FLASK_APP=superset' >> /etc/profile.d/superset.sh
  
export FLASK_APP=superset
sudo -u ubuntu FLASK_APP=superset /home/ubuntu/.local/bin/superset db upgrade
sudo -u ubuntu FLASK_APP=superset /home/ubuntu/.local/bin/superset fab create-admin --username admin --firstname admin --lastname user --email admin@some.co --password 09876 

install_example_data="$(config-get install_example_data)"
if $install_example_data; then
  sudo -u ubuntu FLASK_APP=superset /home/ubuntu/.local/bin/superset load_examples
else
  juju-log -l "WARNING" "Forgoing superset load_examples"
  status-set maintenance "Forgoing superset load_examples $(date +"%H:%M")"
fi
sudo -u ubuntu FLASK_APP=superset /home/ubuntu/.local/bin/superset init

read NUM1 NUM2 <<< $(sudo -u ubuntu FLASK_APP=superset /home/ubuntu/.local/bin/superset version | grep 'Superset')
CLEANED_VER=$(echo "$NUM2" | cut -c 6-)
application-version-set "$CLEANED_VER"

#############################################################################################
# Install the small python client we wrote to interact with Supersets REST API
#############################################################################################
cd /home/ubuntu
pip3 install -e git+https://gitlab.com/en-ser-public/superset-rest-client.git@main#egg=superset-api-client --force-reinstall

juju-log -l "WARNING" "Superset Installer done...rebooting"
status-set maintenance "Superset Installer done...rebooting $(date +"%H:%M")"

juju-reboot
